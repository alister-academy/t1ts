#
# Makefile ESQUELETO
#
# OBRIGATÓRIO ter uma regra "all" para geração da biblioteca e de uma
# regra "clean" para remover todos os objetos gerados.
#
# NECESSARIO adqueuear este esqueleto de makefile para suas necessidades.
#
# OBSERVAR que as variáveis de ambiente consideram que o Makefile está no diretótio "pithread"
# 

CC=gcc
LIB_DIR=./lib/
INC_DIR=./include/
BIN_DIR=./bin/
SRC_DIR=./src/

all: mkbin list waiting pithread scheduler lib_pithread clean_obj

lib_pithread:
	rm -rf $(LIB_DIR)
	mkdir $(LIB_DIR)
	ar crs $(LIB_DIR)libpithread.a $(BIN_DIR)pithread.o $(BIN_DIR)list.o $(BIN_DIR)waiting.o $(BIN_DIR)scheduler.o

scheduler:
	$(CC) -c $(SRC_DIR)scheduler.c -I$(INC_DIR) -Wall
	mv scheduler.o $(BIN_DIR)
	
pithread:
	$(CC) -c $(SRC_DIR)pithread.c -I$(INC_DIR) -Wall
	mv pithread.o $(BIN_DIR)

waiting:
	$(CC) -c $(SRC_DIR)waiting.c -I$(INC_DIR) -Wall
	mv waiting.o $(BIN_DIR)

list:
	$(CC) -c $(SRC_DIR)list.c -I$(INC_DIR) -Wall
	mv list.o $(BIN_DIR)

mkbin:
	mkdir -p $(BIN_DIR)

clean_obj:
	rm -rf $(BIN_DIR)*.o

clean:
	rm -rf $(LIB_DIR)*.a $(BIN_DIR)*.o $(SRC_DIR)*~ $(INC_DIR)*~ *~