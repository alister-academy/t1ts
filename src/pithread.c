#include <stdio.h>
#include <stdlib.h>

#include <stdint.h> // howto-c (as of 2016)
#include <stdbool.h> // because we are not cavemen anymore

#include <string.h>

#include <ucontext.h>
#include "list.h"
#include "utils.h"

#include "pithread.h"
#include "pidata.h"

#include "waiting.h"
#include "scheduler.h"

#define CREDITS_MAX 100
#define CREDITS_MIN 0

#define MAIN_THREAD_CREDITS CREDITS_MAX

// TO-DO: REPLACE RETURN TYPE OF FUNCTIONS
// FROM: INT/INT32_T
// TO: BOOL

// to-do: uptade code with following error test
// if(maintcbcreated == false) "IF(createmaintcb == error) return error"

bool _mainTCB_created = false;

// we bind a context (_exit_context) to a scheduler function (terminateThread)
// this way, everytime a running thread ends its execution
// we switch to the _exit_context, which will call it's binded function
// signaling the scheduler that the current thread ended
ucontext_t* _exit_context;




int32_t _allocExitContext()
{
  _exit_context = (ucontext_t*) calloc(1, sizeof(ucontext_t));
  if(_exit_context == NULL) return ERROR; // calloc error

  _exit_context->uc_link = NULL;
  _exit_context->uc_stack.ss_sp = (uint8_t*) calloc(1, sizeof(SIGSTKSZ));
  if(_exit_context->uc_stack.ss_sp == NULL) {
    STDERR_INFO("Exit context's stack allocation failure \n");
    free(_exit_context); // avoiding memory leak
    return ERROR;
  }
  _exit_context->uc_stack.ss_size = SIGSTKSZ;
  

  // filling other attributes (that are not used by the scheduler)
  getcontext(_exit_context);

  // scheduler function bind
  makecontext(_exit_context, (void (*)(void)) terminateThread, 0, NULL);
  return SUCCESS;
}




/* Creates a control block for the main thread 
*  with the highest priority
*/
int32_t _createMainTCB()
{
  TCB_t* mainThread = (TCB_t*) calloc(1, sizeof(TCB_t));
  if (mainThread == NULL) {
    STDERR_INFO("Main thread control block allocation failure \n");
    return ERROR;
  }


  DEBUG_PRINT(" * Creating main thread control block \n");
  mainThread->state = EXECUCAO;
  mainThread->credCreate = MAIN_THREAD_CREDITS;
  mainThread->credReal = mainThread->credCreate;
  mainThread->tid = 0;

  mainThread->context.uc_link = NULL;
  mainThread->context.uc_stack.ss_sp = (uint8_t*) calloc(1, sizeof(SIGSTKSZ));
  if(mainThread->context.uc_stack.ss_sp == NULL)
  {
    STDERR_INFO("Main thread context memory allocation failure \n");
    free(mainThread); // avoiding memory leak
    return ERROR;
  }
  mainThread->context.uc_stack.ss_size = SIGSTKSZ;

  getcontext(&mainThread->context);
  _mainTCB_created = true;
  setRunningThread(mainThread);    

  return SUCCESS;
}




int32_t picreate (int32_t credCreate, void* (*start)(void*), void *arg)
{
  if(_mainTCB_created == false) _createMainTCB();

  if(_exit_context == NULL) {
    if(_allocExitContext() == ERROR) return ERROR;
  }


  /**************************************************************/
  DEBUG_PRINT(" * Creating thread...   \n");
  /**************************************************************/

  TCB_t* newThread = (TCB_t*) calloc(1, sizeof(TCB_t));
  if(newThread == NULL) return ERROR; // calloc error
  
  // check if credits is within the range
  if(CREDITS_MIN <= credCreate && credCreate <= CREDITS_MAX) {
    newThread->credCreate = credCreate;
    newThread->credReal = credCreate;
  } 
  else {
    // thread credits out of range
    STDERR_INFO("Credits out of range \n");
    free(newThread); // avoiding memory leak
    return ERROR;
  }

  newThread->tid = get_new_TID();
  newThread->state = APTO;
  // set the new thread exit context as the global exit context
  // when this thread ends, it will switch to the _exit_context 
  // and the terminate thread function linked with _exit_context will be called
  // signaling the scheduler that this thread has ended
  newThread->context.uc_link = _exit_context;


  newThread->context.uc_stack.ss_sp = (uint8_t*) calloc(1, sizeof(SIGSTKSZ));
  if(newThread->context.uc_stack.ss_sp == NULL) {
    STDERR_INFO("New thread's stack allocation failure \n");
    free(newThread);
    return ERROR;
  }
  newThread->context.uc_stack.ss_size = SIGSTKSZ;
  
  // fill other things related to the current context 
  // (not used by the scheduler)
  getcontext(&newThread->context);

  // binds the thread's function with it's respective context
  makecontext(&newThread->context, (void (*)(void)) start, 1, (void*) arg);

  if(credCreate == CREDITS_MIN) enqueueExpired(newThread); //to-do: check if needed
  else enqueueActive(newThread);

  return newThread->tid;
}




int32_t piyield(void)
{
  if(_mainTCB_created == false) _createMainTCB();

  TCB_t* runningThread = getRunningThread();
  if(runningThread == NULL) _RAISE_RUNNING_THREAD_ERROR();

  runningThread->state = APTO;
  if(runningThread->credReal >= PRIORITY_DECREMENT) deactivateRunningThread();
  else expireRunningThread();

  // get the next thread with the scheduler
  TCB_t* nextThread;
  nextThread = getNextThread();
  if(nextThread == NULL) _RAISE_NEXT_THREAD_ERROR();
  
  
  DEBUG_PRINT("yielding thread %d (tid) \n", runningThread->tid);
  runThread(runningThread, nextThread);
  return SUCCESS;
}




int32_t piwait(int32_t tid)
{
  if(_mainTCB_created == false) _createMainTCB();

  // check whether target thread exists
  TCB_t* toBeWaited = exists(tid);
  if(toBeWaited == NULL) _RAISE_DOESNT_EXIST_ERROR(tid);


  List* waitingList = getWaiting();
  printf("waiting list %p\n", waitingList);
  // is there any other thread waiting for the target thread?
  if(waitingList_isBeingWaited(waitingList, tid) != NULL) {
    STDERR_INFO("Thread %d is being waited by another thread \n", tid);
    return ERROR;
  }
  // if not, adds the thread to a waiting list
  WaitingInfo* waitingInfo = (WaitingInfo*) calloc(1, sizeof(WaitingInfo));
  if(waitingInfo == NULL) return ERROR; // calloc error

  TCB_t* runningThread = getRunningThread();
  if(runningThread == NULL) _RAISE_RUNNING_THREAD_ERROR();

  waitingInfo->waiting = runningThread->tid; // who is waiting
  waitingInfo->beingWaited = toBeWaited->tid; // target thread

  lockRunningThread();

  list_push_back(waitingList, &waitingInfo);


  // get the next thread with the scheduler
  TCB_t* nextThread = getNextThread();
  if(nextThread == NULL) _RAISE_NEXT_THREAD_ERROR();

  

  DEBUG_PRINT("Thread TID: %d \t waiting for thread TID: %d \n",
                waitingInfo->waiting, 
                waitingInfo->beingWaited);

  runThread(runningThread, nextThread);
  return SUCCESS;
}




int32_t pimutex_init(pimutex_t *mtx)
{
  if(_mainTCB_created == false) _createMainTCB();

  mtx->flag = UNLOCKED;
  mtx->first = NULL; //to-do: remove if we are able to use list struct
  mtx->last = NULL;

  mtx->threads = (List*) calloc(1, sizeof(List));
  if(mtx->threads == NULL) return ERROR;

  if(list_new(mtx->threads, sizeof(TCB_t*), NULL)) //to-do: check if need a free function bind
    return SUCCESS;
  else
    return ERROR;
}




int32_t pilock(pimutex_t *mtx)
{
  if(mtx == NULL) return ERROR;

  if(_mainTCB_created == false) _createMainTCB();


  switch(mtx->flag)
  {
    case UNLOCKED:
    {
      mtx->flag = LOCKED;
      return SUCCESS;
    }

    case LOCKED:
    {
      // if mutex is locked
      // there is another thread in a critical section
      // thus, we must change to another thread
      // to prevent race condition problems
      TCB_t* runningThread = getRunningThread();
      if(runningThread == NULL) _RAISE_RUNNING_THREAD_ERROR();

      TCB_t* nextThread = getNextThread();
      if(nextThread == NULL) _RAISE_NEXT_THREAD_ERROR();

      lockThreadForMutex(mtx, runningThread);
      
      runThread(runningThread, nextThread);
      return SUCCESS;
     } 

    default:
      return ERROR;
  }
}




int32_t piunlock(pimutex_t *mtx)
{
  if(mtx == NULL) return ERROR;
  if(_mainTCB_created == false) _createMainTCB();

  mtx->flag = UNLOCKED;
  unlockMutexThreads(mtx);

  return SUCCESS;
}