#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <assert.h>

#include "utils.h"
#include "scheduler.h"
#include "list.h"

#define SUCCESS 0
#define ERROR -1

int32_t _GLOBAL_TID = 1;

Apts* _active = NULL;
Apts* _expired = NULL;

bool _apts_initialized = false;
#define _INIT_APTS() {_initActive(); _initExpired(); _apts_initialized = true;}

List _locked;
bool _locked_initialized = false;
#define _INIT_LOCKED() {list_new(&_locked, sizeof(TCB_t*), NULL); _locked_initialized = true;}

List _waiting;
bool _waiting_initialized = false;
#define _INIT_WAITING() {list_new(&_waiting, sizeof(WaitingInfo*), NULL); _waiting_initialized = true;}

TCB_t* _running;




int32_t _initActive()
{
  _active = (Apts*) calloc(1, sizeof(Apts));
  if(_active == NULL) return ERROR;

  if(list_new(&(_active->high), sizeof(TCB_t*), NULL) &&
     list_new(&(_active->medium), sizeof(TCB_t*), NULL) &&
     list_new(&(_active->low), sizeof(TCB_t*), NULL))
    return SUCCESS;
  else
    return ERROR;
}




int32_t _initExpired()
{
  _expired = (Apts*) calloc(1, sizeof(Apts));
  if(_expired == NULL) return ERROR;

  if(list_new(&_expired->high, sizeof(TCB_t*), NULL) &&
     list_new(&_expired->medium, sizeof(TCB_t*), NULL) &&
     list_new(&_expired->low, sizeof(TCB_t*), NULL))
    return SUCCESS;
  else
    return ERROR;
}




void runThread(TCB_t* current_thread, TCB_t* next_thread)
{
  assert(next_thread != NULL);

  setRunningThread(next_thread);
  if(current_thread != NULL) {
    swapcontext(&current_thread->context, &next_thread->context);
  }
  else {
    setcontext(&next_thread->context);
  }
}


// get next thread
// using: FIFO with priorities
TCB_t* getNextThread()
{
  if(!_apts_initialized) _INIT_APTS();
  TCB_t* next_thread = NULL;
  
  if(!list_empty(&_active->high))
    next_thread = *(TCB_t**) list_pop_front(&_active->high);
  else
    if(!list_empty(&_active->medium))
      next_thread = *(TCB_t**) list_pop_front(&_active->medium);
    else
      if(!list_empty(&_active->low))
        next_thread = *(TCB_t**) list_pop_front(&_active->low);

  if(next_thread == NULL){
    // when there are no threads in the active group
    // expired group lookup
    if(!list_empty(&_expired->high))
      next_thread = *(TCB_t**) list_pop_front(&_expired->high);
    else
      if(!list_empty(&_expired->medium))
        next_thread = *(TCB_t**) list_pop_front(&_expired->medium);
      else
        if(!list_empty(&_expired->low))
          next_thread = *(TCB_t**) list_pop_front(&_expired->low);
  }


  // if there's a thread in the expired threads group
  // that means all threads ran until they expired
  // this being the case, we just put the expired threads back
  // to the active group
  if(next_thread != NULL) {
    Apts* swap = NULL;
    swap = _active;
    _active = _expired;
    _expired = swap;
  }
  else {
    // if there are no threads at all (on both active and expired groups)
    // there are no threads to run
    // probably an error made by the scheduler
    STDERR_INFO("No thread to run \n");
  }

  return next_thread;
}




void setRunningThread(TCB_t* thread)
{
  _running = thread;
  thread->state = EXECUCAO;
}




// this function is called whenever a thread function ends
int32_t terminateThread()
{
  if(!_waiting_initialized) _INIT_WAITING();


  if(_running == NULL) _RAISE_RUNNING_THREAD_ERROR();
  

  // DEBUG_PRINT("\n\t- A thread de tid %3i terminou de executar. -\n", runningThread->tid);

  // checa se a thread estava sendo esperada por outra
  // se estiver, libera a thread na fila de bloqueados
  WaitingInfo* waitingInfo = waitingList_isBeingWaited(&_waiting, _running->tid);
  if(waitingInfo != NULL) unlockThread(waitingInfo->waiting);

  
  // thread cleanup
  free(_running->context.uc_stack.ss_sp); // frees stack pointer
  free(_running); // frees thread pointer
  

  TCB_t* nextThread = getNextThread();
  if(nextThread != NULL) runThread(NULL, nextThread);

  return SUCCESS;
}




void enqueueActive(TCB_t* thread)
{
  if(!_apts_initialized) _INIT_APTS();
  enqueue(_active, thread);
}




void enqueueExpired(TCB_t* thread)
{
  if(!_apts_initialized) _INIT_APTS();
  enqueue(_expired, thread);
}



// enqueue based on creation credits
void enqueue(Apts* queue, TCB_t* thread)
{
  if(thread->credReal > HIGH_PRIORITY_CREDITS) {
    list_push_back(&queue->high, &thread);
  }
  else {
    if(thread->credReal > MEDIUM_PRIORITY_CREDITS) {
      list_push_back(&queue->medium, &thread);
    }
    else {
      list_push_back(&queue->low, &thread);
    }
  }
}




int32_t get_new_TID()
{
  int32_t tid = _GLOBAL_TID;
  _GLOBAL_TID++;
  return tid;
}




TCB_t* getRunningThread()
{
  return _running;
}


// print thread attributes
bool print_thread(void* thread)
{
  assert(thread != NULL);

  TCB_t* _thread = *(TCB_t**) thread;
  printf("\n");
  printf(" * TID: %d \n", _thread->tid);
  printf(" * State: %d \n", _thread->state);
  printf(" * Initial Credits: %d \n", _thread->credCreate);
  printf(" * Current Credits: %d \n", _thread->credReal);
  printf("\n");
  return false;
}




// decrement the thread's dynamic credits
// enqueue in the correspondent active group queue
int32_t deactivateRunningThread()
{
  if(_running == NULL) _RAISE_RUNNING_THREAD_ERROR();

  _running->credReal -= PRIORITY_DECREMENT;

  enqueueActive(_running);

  _running = NULL; // safety measure

  return SUCCESS;
}




// decrement the thread's dynamic credits
// enqueue in the correspondent expired group queue
int32_t expireRunningThread()
{
  if(_running == NULL) _RAISE_RUNNING_THREAD_ERROR();

  _running->credReal = _running->credCreate;
  
  enqueueExpired(_running);

  _running = NULL; // safety measure

  return SUCCESS;
}




// decrement the current thread's dynamic credits
// insert it in the locked threads list
int32_t lockRunningThread()
{
  if(!_locked_initialized) _INIT_LOCKED();

  if(_running == NULL) _RAISE_RUNNING_THREAD_ERROR();

  _running->credReal -= PRIORITY_DECREMENT;
  _running->state = BLOQUEADO;

  list_push_back(&_locked, &_running);
  return SUCCESS;
}




// takes the thread tid from the locked threads list
// enqueues it in the correspondent active group queue
// and increases its dynamic credits
int32_t unlockThread(int32_t tid)
{
  if(!_locked_initialized) _INIT_LOCKED();

  TCB_t* thread = *(TCB_t**) list_find(&_locked, _compare_tid, &tid);
  if(thread == NULL) _RAISE_DOESNT_EXIST_ERROR(tid);

  thread->credReal += UNLOCK_INCREMENT;
  if(thread->credReal > CREDITS_MAX) 
    thread->credReal = CREDITS_MAX; // saturate

  _running->state = APTO;
  enqueueActive(thread);

  return SUCCESS;
}




// decrement the current thread's dynamic credits
// insert thread in mutex locked threads list
int32_t lockThreadForMutex(pimutex_t *mtx, TCB_t* thread)
{
  if(mtx == NULL) return ERROR;

  _running->credReal -= PRIORITY_DECREMENT;
  if(_running->credReal < 0) _running->credReal = 0;

  thread->state = BLOQUEADO;

  list_push_back(mtx->threads, &thread);

  return SUCCESS;
}




// takes the first thread from the mutex locked threads list
// increment its dynamic credits
int32_t unlockMutexThreads(pimutex_t *mtx)
{
  if(!_locked_initialized) _INIT_LOCKED();

  if(mtx == NULL) return ERROR;

  TCB_t* toBeUnlocked = NULL;
  toBeUnlocked = *(TCB_t**) list_pop_front(mtx->threads);

  if(toBeUnlocked != NULL)
  {
    toBeUnlocked->state = APTO;
    toBeUnlocked->credReal += UNLOCK_INCREMENT;
    if(toBeUnlocked->credReal > CREDITS_MAX) 
      toBeUnlocked->credReal = CREDITS_MAX; // saturate

    enqueueActive(toBeUnlocked);
  }
  return SUCCESS;
}



TCB_t* exists(int32_t tid)
{
  if(!_apts_initialized) _INIT_APTS();
  if(!_locked_initialized) _INIT_LOCKED();
  if(!_waiting_initialized) _INIT_WAITING();

  TCB_t** result = NULL;

  // check if thread is running
  if(_running->tid == tid) return _running;

  // check if thread is in the active group
  // high priority queue
  result = (TCB_t**) list_find(&_active->high, _compare_tid, &tid);
  if(result != NULL) return *result;
  // medium priority queue
  result = (TCB_t**) list_find(&_active->medium, _compare_tid, &tid);
  if(result != NULL) return *result;
  // low priority queue
  result = (TCB_t**) list_find(&_active->low, _compare_tid, &tid);
  if(result != NULL) return *result;

  // check if thread is in the expired group
  // high priority queue
  result = (TCB_t**) list_find(&_expired->high, _compare_tid, &tid);
  if(result != NULL) return *result;
  // medium priority queue
  result = (TCB_t**) list_find(&_expired->medium, _compare_tid, &tid);
  if(result != NULL) return *result;
  // low priority queue
  result = (TCB_t**) list_find(&_expired->low, _compare_tid, &tid);
  if(result != NULL) return *result;

  // check if thread is in the locked threads list
  result = (TCB_t**) list_find(&_locked, _compare_tid, &tid);
  if(result != NULL) return *result;

  // // check if thread is in the waiting threads list
  // result = (TCB_t**) list_find(&_waiting, _compare_tid, &tid);
  // if(result != NULL) return *result;

  return NULL;
}




bool _compare_tid(void* thread, void* tid)
{
  TCB_t* _thread = *(TCB_t**) thread;
  if(_thread == NULL) return false;

  int32_t _tid = *(int32_t*) tid;
  return _thread->tid == _tid ? true : false;
}




Apts* getActive()
{
  return _active;
}




Apts* getExpired()
{
  return _expired;
}




List* getLocked()
{
  return &_locked;
}




List* getWaiting()
{
  return &_waiting;
}