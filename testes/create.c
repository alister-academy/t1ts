#include <stdlib.h>
#include <stdio.h>

#include "../include/pithread.h"

void* myFunc()
{
  printf("myFunc created!\n");
  return NULL;
}

int main()
{
  printf("\n");

  for(int i = 0; i<100; i++)
  {
    int tid = picreate(i, &myFunc, NULL);
    if(tid > 0)  printf("Teste %d - Thread criada com TID: %i\n", i, tid);
    else printf("Nao foi possivel criar thread \n");
  }

  printf("\n");

  return 0;
}