#ifndef __SCHEDULER__
#define __SCHEDULER__

#include "list.h"
#include "utils.h"

#include "pidata.h"
#include "pithread.h"
#include "waiting.h"


#define CREDITS_MAX 100
#define CREDITS_MIN 0

#define HIGH_PRIORITY_CREDITS 80
#define MEDIUM_PRIORITY_CREDITS 40
#define LOW_PRIORITY_CREDITS 0

#define PRIORITY_DECREMENT 10
#define UNLOCK_INCREMENT 20

#define SUCCESS 0 // required cavemen stuff
#define ERROR -1 // required cavemen stuff

#define _RAISE_NEXT_THREAD_ERROR() {STDERR_INFO(" * Unable to get next thread \n"); return ERROR;}
#define _RAISE_RUNNING_THREAD_ERROR() {STDERR_INFO(" * Unable to get running thread \n"); return ERROR;}
#define _RAISE_DOESNT_EXIST_ERROR(tid) {STDERR_INFO(" * Thread %d doesn't exist \n", tid); return ERROR;}

/* Enum de estados possiveis para
*  uma thread
*/
enum {
  CRIACAO = 0,
  APTO = 1,
  EXECUCAO = 2,
  BLOQUEADO = 3,
  TERMINO = 4
};


/* Enum de estados possiveis para
*  um mutex
*/
enum {
  LOCKED = 0,
  UNLOCKED = 1
};


/* Estrutura de lista de aptos
*  contendo tres niveis de prioridade
*  PRIORIDADE ALTA: 100~80
*  PRIORIDADE MEDIA: 80~40
*  PRIORIDADE BAIXA: 40~0
*/
typedef struct Apts {
  List high;
  List medium;
  List low;
} Apts;




/***********************************************/
/********* SCHEDULER MAIN FUNCTIONS ************/

/* DISPATCHER : runThread 
*  o nome so muda para melhorar a semantica
*  a legibilidade do codigo
*
*  Responsavel pela troca de contexto
*  entre thread em execucao e thread
*  escolhida pelo escalonador
*/
void runThread(TCB_t* current_thread, TCB_t* next_thread);

/* SCHEDULER : getNextThread 
*  o nome so muda para melhorar a semantica
*  a legibilidade do codigo
*
*  Responsavel pela escolha da proxima thread
*  atraves de um sistema de filas com multiplas
*  prioridades e politica FILO
*/
TCB_t* getNextThread();

/* Define a thread em execucao e altera 
*  o estado da tcb para EXECUTANDO
*/
void setRunningThread(TCB_t* thread);

/* Responsavel pelo tratamento de uma thread
*  apos o termino de sua execucao
*  libera a memoria e chama o escalonador
*/
int32_t terminateThread();

/* Checks if the thread exists
*/
TCB_t* exists(int32_t tid);
bool _compare_tid(void* thread, void* tid);



/***********************************************/
/********* AptList related functions************/

/* Retorna a lista de aptos ativos */
Apts* getActive();

/* Retorna a lista de aptos expirados */
Apts* getExpired();

/* Insere uma thread na fila de aptos ativos */
void enqueueActive(TCB_t* thread);

/* Insere uma thread na fila de aptos expirados */
void enqueueExpired(TCB_t* thread);

/* Insere uma thread na devida fila de aptos */
void enqueue(Apts* queue, TCB_t* thread);


/***********************************************/
/************* 'Attr' getters ******************/

/* Retorna um TID valido (nao utilizado por outra thread) */
int32_t get_new_TID();

/* Retorna a thread em execucao */
TCB_t* getRunningThread();




/***********************************************/
/************* Print functions *****************/

/* Imprime o conteudo de uma thread */
bool print_thread(void* thread);

/* Imprime a quantidade de elementos em cada lista de aptos */
void print_queues();

/* Imprime a lista de aptos ativos */
void print_actives();




/***********************************************/
/***************** YIELD ***********************/

/* Decrementa o valor especificado em 
*  PRIORITY_DECREMENT da thread em execucao 
*  e a passa para a lista de aptos ativos 
*  para ser escalonada posteriormente
*/
int32_t deactivateRunningThread();

/* Define o valor dos creditos dinamicos da thread 
*  em execucao para o valor dos seus creditos de criacao
*  e a passa para a lista de aptos expirados
*  para ser escalonada posteriormente
*/
int32_t expireRunningThread();




/***********************************************/
/***************** MUTEX ***********************/

int32_t lockThreadForMutex(pimutex_t *mtx, TCB_t* thread);
int32_t unlockMutexThreads(pimutex_t *mtx);




/***********************************************/
/****************** WAIT ***********************/

/* Bloqueia a thread em execucao, passando-a para a lista de espera */
int32_t lockRunningThread();

/* Bloqueia a thread de TID tid, passando-a para a lista de aptos ativos */
int32_t unlockThread(int32_t tid);

/* Retorna a lista threads em espera */
List* getWaiting();

/* Retorna a lista threads em bloqueadas */
List* getLocked();

#endif // __SCHEDULER__